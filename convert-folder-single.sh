#!/usr/bin/env bash

# absolute path to image folder
FOLDER=${1}

# max width
WIDTH=${2}

# max height
HEIGHT=${3}

#resize png or jpg to either height or width, keeps proportions using imagemagick
find ${FOLDER} -not -iname \*-\*x\*.jpg -a -iname \*.jpg -exec bash -c '[[ $(identify -format "%w" "$1") -gt $2 ]] && convert "$1" -verbose -resize $2x$3 ${1%/*}/$(basename "$1" .jpg)-$2x$3.jpg' void {} $WIDTH $HEIGHT \;
find ${FOLDER} -iname \*-\*x\*.jpg -exec bash -c 'cwebp "$1" -o $1.webp' void {} \;
find ${FOLDER} -not -iname \*-\*x\*.png -a -iname \*.png -exec bash -c '[[ $(identify -format "%w" "$1") -gt $2 ]] && convert "$1" -verbose -resize $2x$3 ${1%/*}/$(basename "$1" .png)-$2x$3.png' void {} $WIDTH $HEIGHT \;
find ${FOLDER} -iname \*-\*x\*.png -exec bash -c 'cwebp "$1" -o $1.webp' void {} \;
