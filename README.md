Ce script shell converti les images .jpg ou .png à partir d'un dossier en .webp.
Chaque image est préalablement retaillée dans plusieurs résolutions.
Si une résolution cible est plus élevée que la résolution de l'image initiale, la convertion n'a pas lieu (upscaling, qui occasione nécessairement de la perte de qualité).

Usage : `convert-folder-webp.sh <chemin absolu vers dossier>`

Prérequis :
- commandes : `identify` (taille des images) et `convert` (retailler). 
- commande : `cwebp` pour la convertion des images vers webp

Utilisation :
- le script `convert-folder-single.sh` doit être dans le même dossier que `convert-folder-webp.sh`
- Modifier les résolutions cibles dans le script `convert-folder-webp.sh`