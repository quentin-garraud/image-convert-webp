#!/usr/bin/env bash
./convert-folder-single.sh ${1} 450 300
./convert-folder-single.sh ${1} 600 400
./convert-folder-single.sh ${1} 1200 800
./convert-folder-single.sh ${1} 1920 1280
./convert-folder-single.sh ${1} 2400 1600
